precision lowp float;

uniform float time;
uniform vec2 resolution;

const float PI = 3.141592653589793238;


// "shifted up" sine wave function
// modifies original sine wave output
// from [-1; 1] range to [0.0, 1.0] range
float shSin(float x){
    return (sin(x) + 1.0) / 2.0;
}


// 2d rotation matrix - wikipedia
mat2 rot2d(float angle){
    return mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
}

void main() {
    // these could be moved outside into uniforms
    // so this progressbar becomes customizable from a code
    float stripWidth = 40.0;
    vec3 lightColor = vec3(0.3, 1.0, 0.3);
    vec3 darkenedColor = vec3(0.0, 0.65, 0.2);
    float inclineAngle = PI/4.0;
    float timeScale = 350.0;

    // time in sec from start
    float tsec = time * 0.001 * timeScale;

    // coordinates of a given pixel
    // rotate reference frame by given angle providing inclination to strips
    vec2 uv = (gl_FragCoord.xy + vec2(-tsec, 0.0)) * rot2d(inclineAngle);

    // actually define whether strip should be applied or not
    // 1.0 - dark strip
    // 0.0 - light strip
    float isDarkened = step(0.5, mod(floor(uv.x / stripWidth), 2.0));

    // mix colors
    vec3 res = mix(lightColor, darkenedColor, isDarkened);

    gl_FragColor = vec4(res, 1.0);
}
