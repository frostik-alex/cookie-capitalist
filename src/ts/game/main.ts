import * as PIXI from "pixi.js";
import {AssetEntity, IAssetState} from "./AssetEntity";
import {IAssetUIConfig} from "./ui/AssetUI";
import {IRenderable} from "./interface/IRenderable";
import {IUpdatable} from "./interface/IUpdatable";
import {BalanceUI} from "./ui/BalanceUI";
import {BonusEntity, IBonusState} from "./BonusEntity";
import {IButtonUIConfig} from "./ui/ButtonUI";
import {deepCloneObj} from "../utils/Utils";


const STORAGE_KEY = "COOKIE_GAMESTATE";

export interface IGameState {
    assetStates: IAssetState[],
    bonusStates: IBonusState[],
    balance: number
}

let GAME_STATE: IGameState;


// dumping the GAME_STATE to a localStorage to continue playing after returning back
// TODO:
//  Making this save gamestate to some server needs a check
//  it's possible to make async call synchronous and force player to
//  wait before actually leaving a page until his state is dumped to a server
//  may also be done in a render loop, but I have concerns on the traffic amount in that case


//TODO:
//  Can be generalized to a Serializer class that defines where serialized gamestate goes

window.addEventListener("unload", ()=>{
    let serializedGameState: string = JSON.stringify(GAME_STATE);

    window.localStorage.setItem(STORAGE_KEY, serializedGameState);
});


export function main(pixiApp: PIXI.Application, resources) {
    // add cool background
    let bgSprite = new PIXI.Sprite(
        resources["bg"].texture
    );
    bgSprite.width = 1280;
    bgSprite.height = 720;
    pixiApp.stage.addChild(bgSprite);


    // TODO:
    //  validate old state for correctness here
    let oldState = null;
    try{
        oldState = JSON.parse(window.localStorage.getItem(STORAGE_KEY));
    }catch (e) {
    }

    // TODO: some validation for json structure might be here
    let defaultGameState: IGameState = resources["default_game_state"].data,
        assetsUIConfigs: {[assetId: string]: IAssetUIConfig} = resources["income_ui_configs"].data,

        // TODO: separate interface for BonusUI should be here, it was just faster and more convenient to use button as for POC
        bonusesUIConfigs: {[bonusId: string]: IButtonUIConfig} = resources["bonus_ui_configs"].data;

    // TODO:
    //  used some of my old cloning methods here
    //  might be re-written for much more narrow use-case
    //  you want to clone game state here because otherwise
    //  resources["default_game_state"].data will have a reference to game state
    GAME_STATE = <IGameState>deepCloneObj(oldState ? oldState : defaultGameState);


    // TODO:
    //  firstly, decided to go with interfaces of renderables and updatables
    //  then realized that it would be better to use composition instead of inheritance here
    //  would be better to implement it with some sort of custom ECS pattern.
    let renderables: IRenderable[] = [];
    let updatables: IUpdatable[] = [];


    // create all game entities read from game state.
    GAME_STATE.assetStates.forEach((assetState: IAssetState)=>{
        let assetId: string = assetState.id,
            assetUiConfig: IAssetUIConfig = assetsUIConfigs[assetId];

        let newAssetEntity = new AssetEntity(
            assetId,
            assetUiConfig,
            GAME_STATE
        );
        newAssetEntity.init(pixiApp.stage, resources, pixiApp.ticker);

        renderables.push(newAssetEntity);
        updatables.push(newAssetEntity);
    });

    // TODO:
    //  code duplicate here, might be generalized and united
    //  with AssetEntity to result in "Entity" class.
    GAME_STATE.bonusStates.forEach((bonusState: IBonusState)=>{
        let bonusId: string = bonusState.id,
            bonusUiConfig: IButtonUIConfig = bonusesUIConfigs[bonusId];

        let newBonusEntity = new BonusEntity(
            bonusId,
            bonusUiConfig,
            GAME_STATE
        );
        newBonusEntity.init(pixiApp.stage, resources, pixiApp.ticker);

        renderables.push(newBonusEntity);
    });



    // initialize balance ui
    // can be generalized together with other entities in form of "Entity" that includes a "UIComponent"
    let balanceUI: BalanceUI = new BalanceUI({
        caption : "BALANCE",
        y: 20
    }, GAME_STATE);
    balanceUI.init(pixiApp.stage, resources, pixiApp.ticker);
    renderables.push(balanceUI);



    // game loop
    pixiApp.ticker.add((dts:number)=>{
        // state update first
        updatables.forEach((updatable)=>{
            updatable.update(dts);
        });

        // visual update second
        renderables.forEach((renderable)=>{
            renderable.render();
        });
    });


    (window as any).RESET_GAME_STATE = ()=>{
        window.localStorage.removeItem(STORAGE_KEY);
        GAME_STATE = <IGameState>deepCloneObj(defaultGameState);

        //TODO:
        // need to call reload here for game to hook up to new GameState since all game elements remember old one
        // need to provide GameStateStorage class and return a game state through getter to make everything reactive to skip reloading part
        // or use Redux
        window.location.reload();
    }
}