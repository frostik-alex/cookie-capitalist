import * as PIXI from "pixi.js";
import {AssetUI, IAssetUIConfig} from "./ui/AssetUI";
import {IVisualEntity} from "./interface/IVisualEntity";
import {IRenderable} from "./interface/IRenderable";
import {IUpdatable} from "./interface/IUpdatable";
import {IGameState} from "./main";


export interface IAssetState {
    id: string;
    cost: number;
    incomePerUnit: number;
    amount: number;
    duration: number;
    active: boolean;
    activationTime: number;
    hasManager: boolean;
}


// naming confusion here - this class describe "business asset" not "game asset" like picture etc.
// should have picked another name but it was too late as some point, should be enough for POC (i hope)
export class AssetEntity implements IVisualEntity, IRenderable, IUpdatable {

    private _id: string;
    private _uiConfig: IAssetUIConfig;
    private _ui: AssetUI;


    private _gameState: IGameState;
    private _assetState: IAssetState;

    constructor(id: string, uiConfig: IAssetUIConfig, gameState: IGameState) {
        this._id = id;
        this._uiConfig = uiConfig;

        // store whole gameState to gain access to balance
        // there is no reason to expose whole gamestate for this only
        // one way to bypass that is to introduce Redux like actions
        this._gameState = gameState;

        // storing "state slice" to skip searching for it each time we need an access
        this._assetState = gameState.assetStates.find((asset: IAssetState) => {
            return asset.id === this._id;
        });
    }


    init(parent: PIXI.Container, resources: Partial<Record<string, PIXI.LoaderResource>>, ticker: PIXI.Ticker): void {
        let uiConfig = this._uiConfig;

        this._ui = new AssetUI(uiConfig);
        this._ui.init(parent, resources, ticker);

        this._ui.setIconCallback(this.activate.bind(this));

        this._ui.buyButton.setClickCallback(() => {
            this._gameState.balance -= this._assetState.cost;
            this._assetState.amount++;
            this._assetState.cost *= 1.35;
        })
    }


    render() {
        let canPurchase: boolean = this._gameState.balance >= this._assetState.cost;

        // update icon
        this._ui.setIconEnabled(this._assetState.amount > 0 && !this._assetState.active);

        // update button
        this._ui.buyButton.setText(this._assetState.cost.toFixed(2));
        this._ui.buyButton.setEnabled(canPurchase);

        // update progress bar
        let isActive = this._assetState.active;
        this._ui.progressBar.setIsRunning(isActive);
        if (isActive) {
            this._ui.progressBar.setProgress(
                (Date.now() - this._assetState.activationTime) * 0.001 / this._assetState.duration
            );

        } else {
            this._ui.progressBar.setProgress(0.0)
        }

        // update amount
        this._ui.setAmount(this._assetState.amount);

        //update total income
        this._ui.setTotalIncome(this._assetState.incomePerUnit * this._assetState.amount);
    }


    private activate(): void {
        if (this._assetState.amount === 0) {
            return;
        }

        if (this._assetState.active) {
            return;
        }


        this._assetState.active = true;
        this._assetState.activationTime = Date.now();
    }

    update(dts: number) {
        let currTime: number = Date.now(),
            timePassedSinceActivated: number = (currTime - this._assetState.activationTime) * 0.001, // in sec
            // number of full cycles passed from activation time till this update call
            cyclesPassed: number = Math.floor(timePassedSinceActivated / this._assetState.duration);


        if (this._assetState.active && cyclesPassed > 0) {
            // when asset has a manager - we don't turn it off, we award all money for full cycles and then shift activation time
            if (this._assetState.hasManager) {
                // award all money gained
                this._gameState.balance += cyclesPassed * this._assetState.amount * this._assetState.incomePerUnit;

                // shift activation time
                this._assetState.activationTime += cyclesPassed * this._assetState.duration * 1000;
            } else {
                // if asset doesn't have manager - award money for one cycle only and disable it
                this._gameState.balance += this._assetState.amount * this._assetState.incomePerUnit;

                this._assetState.active = false;
            }
        }

        if (!this._assetState.active && this._assetState.hasManager) {
            this.activate();
        }

    }
}
