export interface IUpdatable {
    update(dts: number);
}