import * as PIXI from "pixi.js"
import {FancyProgressBar, IFancyProgressBarConfig} from "./fancyProgressBar/FancyProgressBar";
import {ButtonUI} from "./ButtonUI";
import {IVisualEntity} from "../interface/IVisualEntity";


// constant config that is shared between all instances of AssetUI
export const DEFAULT_PB_CONFIG: IFancyProgressBarConfig = {
    width: 200,
    height: 20,
    initProgress: 0.1,
    fragShaderName: "fancy_pb"
};


//TODO:
// this can also be generalized to some interface to
// to make it possible to override these values
export const ASSET_UI_CONST = {
    icon: {
        x: 60,
        y: 60,
        width: 100,
        height: 100
    },
    progressBar: {
        icon_offset_x: 20,
        icon_top_offset_y: 5
    },
    buyButton: {
        position:{
            x: 180,
            y: 100
        },
        width: 140,
        height: 60,
        captionText: "BUY",
        textures:{
            enabled: "buybtn_enabled",
            disabled: "buybtn_disabled"
        }

    },
    caption_offset_y: 30,
    frameImgTexture: "asset_frame",
    width: 420,
    height: 200
};


export interface IAssetUIConfig {
    caption: string,
    iconTextureName: string,
    x: number,
    y: number
    progressBar?: IFancyProgressBarConfig
}

export class AssetUI implements IVisualEntity{
    private _config: IAssetUIConfig;
    private _backgroundFrame: PIXI.Sprite;
    private _caption: PIXI.Text;
    private _icon: PIXI.Sprite;
    private _amountCaption: PIXI.Text;
    private _totalIncomeCaption: PIXI.Text;


    public progressBar: FancyProgressBar;
    public buyButton: ButtonUI;
    public root: PIXI.Container;

    constructor(config: IAssetUIConfig) {
        this._config = config;
    }

    init(stage: PIXI.Container, resources: Partial<Record<string, PIXI.LoaderResource>>, ticker: PIXI.Ticker): void {
        this.root = new PIXI.Container();

        // initialize background frame
        this._backgroundFrame = new PIXI.Sprite(
            resources[ASSET_UI_CONST.frameImgTexture].texture
        );
        this.root.addChild(this._backgroundFrame);


        // init caption text
        this._caption = new PIXI.Text(this._config.caption);
        this._caption.position.y = ASSET_UI_CONST.caption_offset_y;
        this._caption.position.x = this._backgroundFrame.width/2 - this._caption.width/2;
        this.root.addChild(this._caption);

        // initialize _icon
        this._icon = new PIXI.Sprite(
            resources[this._config.iconTextureName].texture
        );
        this._icon.position.set(ASSET_UI_CONST.icon.x, ASSET_UI_CONST.icon.y);
        this._icon.width = ASSET_UI_CONST.icon.width;
        this._icon.height = ASSET_UI_CONST.icon.height;
        this.root.addChild(this._icon);


        // initialize initProgress bar
        let progressBarConfig: IFancyProgressBarConfig = {
            ...DEFAULT_PB_CONFIG,
            ...(this._config.progressBar? this._config.progressBar: {})
        };

        this.progressBar = new FancyProgressBar(progressBarConfig);
        this.progressBar.init(this.root, resources, ticker);
        this.progressBar.position.set(
            this._icon.position.x + this._icon.width + ASSET_UI_CONST.progressBar.icon_offset_x,
            this._icon.position.y + ASSET_UI_CONST.progressBar.icon_top_offset_y
        );


        // initialize buy button
        this.buyButton = new ButtonUI(ASSET_UI_CONST.buyButton);
        this.buyButton.init(this.root, resources, ticker);


        // init amount caption
        this._amountCaption = new PIXI.Text("0");
        this._amountCaption.anchor.set(0.5);
        this._amountCaption.position.x = this._icon.position.x + this._icon.width/2;
        this._amountCaption.position.y = this._icon.position.y + this._icon.height + 10;
        this.root.addChild(this._amountCaption);

        // init total income caption
        this._totalIncomeCaption = new PIXI.Text("0");
        this._totalIncomeCaption.anchor.set(1.0, 0);
        this._totalIncomeCaption.position.x = this._backgroundFrame.width - 60;
        this._totalIncomeCaption.position.y = ASSET_UI_CONST.buyButton.position.y;
        this.root.addChild(this._totalIncomeCaption);

        stage.addChild(this.root);

        this.root.position.set(this._config.x, this._config.y);
    }

    setIconEnabled(enabled):void{
        this._icon.interactive = enabled;
    }

    setIconCallback(cb): void{
        this._icon.on("pointerdown", cb);
    }

    setAmount(amount: number){
        this._amountCaption.text = amount.toString();
    }

    setTotalIncome(totalIncome:number): void{
        this._totalIncomeCaption.text = totalIncome.toFixed(2);
    }
}