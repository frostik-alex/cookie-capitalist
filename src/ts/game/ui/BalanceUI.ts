import * as PIXI from "pixi.js";

import {IRenderable} from "../interface/IRenderable";
import {IVisualEntity} from "../interface/IVisualEntity";
import {IGameState} from "../main";


export interface IBalanceUIConfig {
    caption: string,
    y: number
}

export class BalanceUI implements IVisualEntity, IRenderable{
    private _balanceText: PIXI.Text;
    private _config: IBalanceUIConfig;
    private _gameState: IGameState;

    constructor(config: IBalanceUIConfig, gameState: IGameState) {
        this._config = config;
        this._gameState = gameState;
    }


    init(parent: PIXI.Container, resources: Partial<Record<string, PIXI.LoaderResource>>, ticker: PIXI.Ticker): void {
        this._balanceText = new PIXI.Text("");
        this._balanceText.anchor.set(0.5, 0);
        this._balanceText.position.set(parent.width/2, this._config.y);
        this._balanceText.style.fill = "#ffffff";

        parent.addChild(this._balanceText);
    }


    render() {
        this._balanceText.text = `${this._config.caption}: ${this._gameState.balance.toFixed(2)}`;
    }

}