import * as PIXI from "pixi.js";
import {IVisualEntity} from "../interface/IVisualEntity";


export interface IButtonUIConfig {
    position: {
        x: number,
        y: number
    },
    width: number,
    height: number,

    captionText: string,

    textures:{
        enabled: string,
        disabled: string
    }
}

/**
 * Generic button class implementation with two basic states - enabled and disabled.
 *
 * TODO: hover/pressed states may be here
 */
export class ButtonUI implements IVisualEntity{
    private _config: IButtonUIConfig;
    private _sprite: PIXI.Sprite;

    private _enabledTexture: PIXI.Texture;
    private _disabledTexture: PIXI.Texture;

    private _clickCb: ()=>void;

    private _isEnabled: boolean;
    private _captionText: PIXI.Text;
    private _root: PIXI.Container;


    constructor(config: IButtonUIConfig) {
        this._config = config;
    }

    init(parent: PIXI.Container, resources: Partial<Record<string, PIXI.LoaderResource>>, ticker: PIXI.Ticker): void {
        this._root = new PIXI.Container();

        this._sprite = new PIXI.Sprite();
        this._sprite.interactive = true;
        this._sprite.width = this._config.width;
        this._sprite.height = this._config.height;

        this._sprite.on("pointerdown", this.internalClickHandler.bind(this));

        this._enabledTexture = resources[this._config.textures.enabled].texture;
        this._disabledTexture = resources[this._config.textures.disabled].texture;
        this.setEnabled(false);


        this._root.addChild(this._sprite);


        // TODO: text size is not adaptive but it can be so
        this._captionText = new PIXI.Text(this._config.captionText);
        this._captionText.anchor.set(0.5);
        this._captionText.position.set(
            this._sprite.width/2,
            this._sprite.height/2
        );
        this._root.addChild(this._captionText);

        this._root.position.copyFrom(this._config.position);

        parent.addChild(this._root);
    }

    private internalClickHandler(): void{

        if(!this._isEnabled){
            return;
        }

        if(!this._clickCb){
            return;
        }

        this._clickCb.apply(this, arguments)
    }

    setEnabled(newEnabledState: boolean):void {
        this._isEnabled = newEnabledState;
        this._sprite.texture = this._isEnabled ? this._enabledTexture : this._disabledTexture;
    }

    setClickCallback(cb: ()=>void):void{
        this._clickCb = cb;
    }

    setText(text: string):void{
        this._captionText.text = text;
    }
}