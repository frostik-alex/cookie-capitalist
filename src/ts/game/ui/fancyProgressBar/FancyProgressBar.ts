import * as PIXI from "pixi.js"
import {ISize} from "../../../utils/Utils";
import {IVisualEntity} from "../../interface/IVisualEntity";


export interface IFancyProgressBarConfig{
    width: number,
    height: number,
    fragShaderName: string,
    initProgress: number
}

/**
 * Class that implements a ProgressBar object on base of PIXI.Container and fragment shader to display graphics.
 *
 * Usage of shaders here is quite redundant, still those are useful to display infinitely recurrent patterns.
 */
export class FancyProgressBar implements IVisualEntity{

    private _config: IFancyProgressBarConfig;

    private _view: PIXI.Container;
    private _originalSize: ISize;

    private _dummySprite: PIXI.Sprite;
    private _filter: PIXI.Filter;
    private _runningStartTime: number;

    private _progressPercentage: number;
    private _isRunning: boolean;

    // provide accessors to be able to change it's position
    get position(): PIXI.Point{
        return this._view.position;
    }

    get width(): number{
        return this._originalSize.width;
    }

    get height(): number{
        return this._originalSize.height;
    }

    constructor(config: IFancyProgressBarConfig) {
        this._config = config;
    }


    public setIsRunning(isRunning: boolean):void{
        this._isRunning = isRunning;
    }

    public setProgress(percentage: number):void{
        // clamp value between [0;1]
        let newVal = Math.max(Math.min(1.0, percentage), 0.0),
            newWidth = this._originalSize.width * newVal;

        this._progressPercentage = newVal;

        this._dummySprite.width = newWidth;
    }

    init(parent: PIXI.Container, resources: Partial<Record<string, PIXI.LoaderResource>>, ticker: PIXI.Ticker): void {
        let config = this._config,
            fragShaderData: string = resources[config.fragShaderName].data;

        if (!fragShaderData || fragShaderData.length === 0) {
            throw new Error("Can't create initProgress bar. Wrong shader data provided")
        }

        this._runningStartTime = Date.now();
        this._originalSize = {
            width: config.width,
            height: config.height
        };

        // initialize main container and filter
        this._view = new PIXI.Container();
        this._filter = new PIXI.Filter(null, fragShaderData, {
            time: 0,
            resolution: {x: this._view.width, y: this._view.height},
            position: {x: this._view.position, y: this._view.position}
        });
        this._view.filters = [this._filter];


        // initialize sprite that will be actually rendered
        this._dummySprite = new PIXI.Sprite();
        this._dummySprite.position.x = 0;
        this._dummySprite.position.y = 0;
        this._dummySprite.width = config.width;
        this._dummySprite.height = config.height;
        this._view.addChild(this._dummySprite);


        ticker.add(()=>{
            if(this._isRunning){
                this._filter.uniforms.time = Date.now() - this._runningStartTime;
            }
            this._filter.uniforms.resolution.x = this._view.width;
            this._filter.uniforms.resolution.y = this._view.height;
        });

        parent.addChild(this._view);
        this.setProgress(this._config.initProgress);
        this.setIsRunning(true);
    }

}