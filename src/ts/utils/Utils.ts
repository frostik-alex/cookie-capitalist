import * as PIXI from "pixi.js";

export interface ISize {
    width: number,
    height
}

export function getDocumentViewportSize(): ISize {
    let cw = document.documentElement.clientWidth, // client width
        ch = document.documentElement.clientHeight, // client height

        iw = window.innerWidth, // inner width
        ih = window.innerHeight; // inner height

    return {
        width: cw < iw ? iw : cw,
        height: ch < ih ? ih : ch
    }
}

export function wrapPixiLoader(loader: PIXI.Loader): Promise<Partial<Record<string, PIXI.LoaderResource>>> {
    return new Promise((resolve, reject) => {
        loader.onError.add(function () {
            // arguments here will contain arguments from "onError" callback
            reject(Array.from(arguments))
        });

        loader.load((loader: PIXI.Loader, resources: Partial<Record<string, PIXI.LoaderResource>>) => {
            resolve(resources);
        });
    });
}


export function deepCloneObj(objToClone: { [key: string]: any }): { [key: string]: any } {

    if (typeof objToClone !== "object") {
        return objToClone;
    }

    let cloned: { [key: string]: any } = {};

    for (let fieldName in objToClone) {
        if (typeof objToClone[fieldName] === "object") {

            if (objToClone[fieldName] instanceof Array) {
                cloned[fieldName] = [];
                for (let i = 0; i < objToClone[fieldName].length; i++) {
                    cloned[fieldName][i] = deepCloneObj(objToClone[fieldName][i]);
                }
            } else {
                cloned[fieldName] = deepCloneObj(objToClone[fieldName]);
            }

        } else {
            cloned[fieldName] = objToClone[fieldName];
        }
    }

    return cloned;
}

