import * as PIXI from "pixi.js"
import {ISize} from "./utils/Utils";
import "stats.js";
import {main} from "./game/main";
import {ViewportWrapper} from "./core/ViewportWrapper";
import {AssetsLoader} from "./core/AssetsLoader";


export const RENDER_RESOLUTION: ISize = {
    width: 1280,
    height: 720
};

var PIXI_APP: PIXI.Application,
    VIEWPORT_WRAPPER: ViewportWrapper,
    ASSETS_LOADER: AssetsLoader,
    STATS: Stats;


function initAppView() {
    STATS = new Stats();
    document.body.appendChild(STATS.dom);

    // init PIXI Application object that will initialize renderer as well
    PIXI_APP = new PIXI.Application({
        autoStart: false,
        ...RENDER_RESOLUTION
    });
    PIXI_APP.resizeTo = null;

    // add STATS.begin(); as a first callback to ticker
    // may be "strip blocked" in production version
    PIXI_APP.ticker.add(() => {
        STATS.begin();
    });

    // get viewport dic and append target canvas to it
    let viewportDiv: HTMLElement = document.getElementById("viewport");
    viewportDiv.appendChild(PIXI_APP.view);

    // initialize viewport div object with a wrapper class
    // this will attach onresize callback and call "resize" to an
    VIEWPORT_WRAPPER = new ViewportWrapper(RENDER_RESOLUTION, viewportDiv);
}

async function appInit() {

    initAppView();

    ASSETS_LOADER = new AssetsLoader({
        assetsConfigPath: "config/assets_config.json"
    });

    let resources = await ASSETS_LOADER.load();

    // game main js - all game entities stuff is initialized here
    main(PIXI_APP, resources);

    // add STATS.end(); as a last callback to measure time correctly
    PIXI_APP.ticker.add(() => {
        STATS.end();
    });

    // actually start render loop
    PIXI_APP.start();
}

window.addEventListener("load", appInit);
