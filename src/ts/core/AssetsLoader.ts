import * as PIXI from "pixi.js"
import {wrapPixiLoader} from "../utils/Utils";


export interface IAssetsLoaderConfig {
    assetsConfigPath: string;
}

export class AssetsLoader {
    private _configLoader: PIXI.Loader;
    private _assetsLoader: PIXI.Loader;

    constructor(config: IAssetsLoaderConfig) {
        this._configLoader = new PIXI.Loader();
        this._assetsLoader = new PIXI.Loader();

        this._configLoader.add("assetsConfig", config.assetsConfigPath);
    }

    async load(): Promise<Partial<Record<string, PIXI.LoaderResource>>>{
        let assetsConfig: object;

        // load all assets config before actually loading them
        try{
            assetsConfig = (await wrapPixiLoader(this._configLoader))["assetsConfig"].data;
        }catch (e) {
            throw new Error("Could not load assets config");
        }

        // add all images from configuration to load queue
        for (let assetName in assetsConfig) {
            if (!assetsConfig.hasOwnProperty(assetName)) {
                continue;
            }
            this._assetsLoader.add(assetName, assetsConfig[assetName]);
        }

        return wrapPixiLoader(this._assetsLoader);
    }

}