# Cookie capitalist


##Description
This is an example of development version of idle game that operates on a front-end side.  (Adventure Capitalist simplified clone)

# Disclaimer

This product is in unpolished state "code" and "architecture"-wise. Most questionable and smelly parts are marked with TODOs.

This product also doesn't have unit tests at all, but they are crucial for "production ready code".

This product doesn't implement any build system and does not produce any deployable folder/zip. 
Please use provided script to run it.

## Running a game 
Install dependencies

`npm install`

Run start script
 
`npm start`


To reset an in-game state, please use 
`RESET_GAME_STATE();` global function from Dev-tools console when game is opened.

## Game features
- 3 different businesses
- "Manager" and "Triple income" bonuses for each.
- Resizable viewport, ready to be included into an iFrame
- Game simulates "running in idle" and updates all states accordingly when you come back after some time.


## Technical and architectural reasoning / personal thoughts


### index.ts, ViewportWrapper and AssetsLoader

These files represent the core layer of a game that also might include "loaders", "integrations" etc.
Usually this is made as a separate product independently from a game part.

ViewportWrapper is essential for game to be runnable on all platforms and fit each screen aspect ratio.

### Redux and why it's not used here (yet)

This game seems to fit Redux very well. Nevertheless I have zero experience with redux and reactive ui programming.

I've tried attaching it from the very beginning, but failed due to lack of a "bigger picture" on how to organize Reducers and Actions in a better way.

Another concern against using Redux for this particular game is that idea of Redux seem(i might be very wrong here) to create a clone of a state for each state change (action).
This looks redundant for this case due to the fact that any balance change would generate state change and soon we may run out of memory(needs to be checked).

Probably there is an elegant way to make it work with Redux, but lack of experience with it prevents me from seeing it straight away.

### IRenderable and IUpdatable

I decided to go with most generic impl and see what approach matches this game best "on the go" while implementing very basics.

It appeared that some hand-made form of ECS might fit better here. If I write this game again, I'd use ECS for game structure.

From my experience I used ECS for real-time games only, so I wasn't sure this game will utilize it well. Thought it might be an overshoot.

### PIXI and UI - might be replaced with UI on Spine (or any other 2d editor with PIXI export support)

Easier way to organize UI is to make it in Spine or other editor with PIXI export.

In that way artist who makes that Spine object sees how game will look like and can make tweaks to it independently from a developer.

An developers are focused on the business logic mostly.

### FancyProgressBar with use of shader for render

Just for fun. Could be done with an animated sprite/recurrent texture/clipped animation. 

## Trade-offs made and left out parts

Parts that have not been implemented: 
- Build/CI - script needed that will bundle a game to a runnable folder with proper links to assets etc.
- Tests - unit tests missing
- Redux - can be a fit here, more time required
- UI framework - UI elements may be organized in some form of UI framework with configs. This framework is to be integrated into game structure.
- Form of ECS - to be used instead of entities inheriting different interfaces.
- Storage/DB class - Some generic class to store GameState to a given destination. Now is impl in main.ts
 
Most smelly/questionable parts are marked with TODOs through the code.
 
 
